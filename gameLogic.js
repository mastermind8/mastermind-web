//import data from "./level.json"

/* MASTERMIND WEB
 * ---------------
 * JavaScript Game Logic
 * Author: Niklas Schachl
 * Date: 02.02.2023
 * Version: 1.3
 */

const colours = ["#00ffff", "#ff0000", "#00ff00", "#0000ff", "#ff1493", "#ffff00"];     //all the colours the system can use for patterns
var usedColours = [];                                                                   //temporary store the colours already used in the pattern
var hardMode = false;                                                                   //Variable to store if the user has activated hardMode, false by default

//Array, which holds the generated colour pattern
var pattern = [];// = new Array(4);
var pattern_try = new Array(4);     //Array which holds the pattern the user has created
var id_divClicked;                              //holds the ID of the button that should be colored
var activeRow = 1;                              //holds the current row the user can interact with
var lastClicked = 0;                            //holds the id/index of the last clicked item

//ID for the rows
var row1 = ["1", "2", "3", "4"];
var row2 = ["5", "6", "7", "8"];
var row3 = ["9", "10", "11", "12"];
var row4 = ["13", "14", "15", "16"];
var row5 = ["17", "18", "19", "20"];
var row6 = ["21", "22", "23", "24"];
var row7 = ["25", "26", "27", "28"];
var row8 = ["29", "30", "31", "32"];

var colourPositionHint = [];    //array to store the IDs of the clicked buttons to display the hints later

var currentScore;
var highscore;
var music;

//called when the game is started; initialises all the necessary parts
function init()
{
    console.log("[ at.mastermind8.web.init ][ *** ] Initializing game...");

    //reset the active row to the first row
    activeRow = 1;

    //Make sure that the pattern-array is empty
    pattern = [];

    //Make sure that the array with the used colours is empty
    usedColours = [];

    //get the level of the game
    console.log("[ at.mastermind8.web.init.hardMode ][ *** ] Trying to obtain hardMode-setting from LocalStorage...")
    hardMode = localStorage.getItem("at.mastermind8.web.hardMode");
    if(hardMode == null)
    {
        console.log("[ at.mastermind8.web.init.hardMode ][ WARN ] hardMode-setting couldn't be obtained, using default setting instead");
        hardMode = false;
    }
    console.log("[ at.mastermind8.web.init.hardMode ][ OK ] hardMode is set to " + hardMode);

    //get the highscore
    highscore = localStorage.getItem("at.mastermind8.web.highscore");
    if(highscore == null)
    {
        console.log("[ at.mastermind8.web.highscore ][ WARN ] highscore couldn't be obtained, using default setting instead");
        highscore = 0;
    }
    else if (hardMode == "false")
    {
        hardMode = false;
    }
    else if (hardMode == "true")
    {
        hardMode = true;
    }
    console.log("[ at.mastermind8.web.highscore ][ OK ] highscore is set to " + highscore);

    //set the highscore text
    let div_highscore = document.getElementById("div_highscore");
    div_highscore.textContent = highscore;

    //get the backgroundMusic
    console.log("[ at.mastermind8.web.init.hardMode ][ *** ] Trying to obtain background music-setting from LocalStorage...")
    music = localStorage.getItem("at.mastermind8.web.music");
    if(music == null)
    {
        console.log("[ at.mastermind8.web.music ][ WARN ] background music couldn't be obtained, using default setting instead");
        music = true;
    }
    console.log("[ at.mastermind8.web.music ][ OK ] music is set to " + music);

    //activate / deactivate background music
    let audio = document.getElementById("audio1");
    if(music == "false")
    {
        console.log("[ at.mastermind8.web.music ][ OK ] disabling music");

        audio.pause();
        audio.currentTime = 0;
    }
    else if (music == "true" || music == true)
    {
        console.log("[ at.mastermind8.web.music ][ OK ] Enabling music");

        //check if audio is already playing (in case someone clicked on "play again"
        console.log("[ at.mastermind8.web.music ][ DEBUG ] audio.paused: " + audio.paused);
        if(audio.paused)
        {
            audio.play();
        }
    }

    //generate a random colour pattern from the available colours
    console.log("[ at.mastermind8.web.init.generatePattern ][ *** ] Generating pattern, hardMode is set to " + hardMode + "...");
    for (let i = 0; i < 4; i++)
    {
        let randomColour = getRandomColour();   //get a random colour
        pattern.push(randomColour);             //store the random colour in the pattern-array
    }
    console.log("[ at.mastermind8.web.init.generatePattern ][ OK ] Generated pattern");

    //reset current score
    currentScore = 0;
}

//function to get a random colour from the available colours and return its hex-code
function getRandomColour()
{
    let colourCorrect = true;   //variable to store if the selected colour is already in the pattern
    let randomColour;           //variable to store the randomly selected colour
    let index;                  //index of the colours-array

    //generate a new random colour as long as the generated colour isn't "correct"
    do
    {
        colourCorrect = true;
        //generate a random number from 0 to 5 and store it
        index = Math.floor(Math.random() * (5));//Math.trunc((Math.random() * 7) - 1);
        console.log("[ at.mastermind8.web.getRandomColour ][ INFO ] index: " + index);

        randomColour = colours[index];  //get the colour-value from the colours-array at the randomly selected index

        //check if the hardMode is deactivated. If it is deactivated, it needs to be checked if no colour is used more than one time
        if (hardMode === false)
        {
            //check if any colours are already in use
            if (usedColours.length !== 0)
            {
                //console.log(usedColours);

                //check if the generated colour already exists, if so, the colour is not "correct", so don't use it and go through the loop another time
                if (usedColours.includes(index))
                {
                    colourCorrect = false;
                }
            }
        }
    }
    while (colourCorrect !== true) 

    usedColours.push(index);    //store the colours that are already used
    return randomColour;        //return the hex-value of the generated colour
}

/*
    function to display a colourPicker when the user clicks on a button
    id: the ID of the button that activated this event
 */
function action_displayColourPicker(id)
{
    id_divClicked = id;                             //store the ID of the button that got clicked
    let div_clicked = document.getElementById(id);  //get the button that triggered the event

    //DEBUG
    let activeRowString = "row-" + activeRow;

    let id_int = id.match(/(\d+)/); //get the integer value of the ID

    //DEBUG
    //console.log("[ DEBUG ][ action_displayColourPicker ] lastClicked: " + lastClicked);
    //console.log("[ DEBUG ][ action_displayColourPicker ] id (only number): " + id_int[0]);
    //console.log("[ DEBUG ][ action_displayColourPicker ] id difference: " + (id_int[0] - lastClicked));

    //Every button has (in addition to its ID) a CSS-class that defines the row the button is in. Check if the button that triggered the event is part of the active row and is therefore allowed to be coloured
    if(div_clicked.classList.contains(activeRowString))
    {
        let container = document.getElementById("container_colourPicker");  //get the container that acts as the colourPicker
        container.style.visibility = "visible";                                      //show the colourPicker by making the container-element visible
        console.log("[ at.mastermind8.web.action_displayColourPicker ][ DEBUG ] Display colourPicker");
    }
    lastClicked = id_int[0] - 1;    //save the element that got clicked as the lastClicked element
}

/*
    function to colour a game element with the colour of the user's choice
    id: the ID of the button that activated this event
 */
function action_pickColour(id)
{
    //console.log("[ DEBUG ][ action_pickColour ] id: " + id);
    //console.log("[ DEBUG ][ action_pickColour ] lastClicked: " + lastClicked);
    //console.log(id);

    let buttonClicked = document.getElementById(id);        //get the colorButton that got clicked and retrieve its color
    let styles = window.getComputedStyle(buttonClicked);    //get the style of the colour-button that got clicked
    let colour = styles.backgroundColor;                    //retrieve the colour of the button
    colour = convertColourToHex(colour);                    //convert the colour to hex-code

    pattern_try[lastClicked % 4] = colour;                  //add the colour to the guess-pattern at the position of the element that got coloured

    console.log("[ at.mastermind8.web.action_pickColour ][ OK ] Added colour " + pattern_try[lastClicked % 4] + "to pattern_try at position " + lastClicked % 4);
    console.log(colour);

    let divClicked = document.getElementById(id_divClicked);    //get the ID of the button that should be colored
    divClicked.style.backgroundColor = colour;                  //set the (background-)colour of the button that should be coloured

    //get the container for the color-picker and make it invisible
    let container = document.getElementById("container_colourPicker");
    container.style.visibility = "hidden";

    //add current id_divClicked to array to use it later to display the hints
    colourPositionHint[colour] = id_divClicked;
    colourPositionHint.push({"colour" : colour, "div_id" : id_divClicked});
    console.log("[ at.mastermind8.web.action_pickColour ][ DEBUG ][ action_pickColour ] colourPositionHint: ");
    console.log(colourPositionHint);

    /*
    //display the hints
    displayPositionHint(colour);
    displayColourHint(colour);
     */

    //console.log("[ DEBUG ] pattern_try.length: " + pattern_try.length);

    //Loop through the user-created pattern array and store how many fields are already coloured
    let rowDoneCount = 0;
    for (let i = 0; i < pattern_try.length; i++)
    {
        if(pattern_try[i] != null)
        {
            /*
            console.log("[ DEBUG ][ action_pickColour ] pattern_try: ");
            pattern_try.forEach(item => {
                console.log(item);
            })
            */
            console.log("[ at.mastermind8.web.action_pickColour ][ DEBUG ][ action_pickColour ] pattern_try[i]: " + pattern_try[i]);
            rowDoneCount++;
        }
        console.log("[ at.mastermind8.web.action_pickColour ][ DEBUG ][ action_pickColour ] rowDoneCount: " + rowDoneCount);
    }

    //Check if all four fields in the current row are coloured by the user
    if(rowDoneCount >= 4)
    {
        //Save which of the points match
        let matchingPoints = new Array(4);

        //loop through the generated pattern to check if every colour matches the user-picked colour
        for(let i = 0; i < pattern.length; i++)
        {
            //check if the user-picked pattern at the index i is the same as the generated pattern at the index
            if(pattern_try[i] === pattern[i])
            {
                console.log("[ at.mastermind8.web.action_pickColour ][ INFO ] Patterns match!");
                console.log(pattern_try[i] + " <=> " + pattern[i] + " | " + i);

                matchingPoints[i] = true;   //save that the colours at the  index i match
            }
            else
            {
                console.log("[ at.mastermind8.web.action_pickColour ][ INFO ] Patterns don't match!");
                console.log(pattern_try[i] + " <=> " + pattern[i] + " | " + i);

                matchingPoints[i] = false;  //save that the colours at the  index i don't match
            }
        }

        //add up the current score
        currentScore++;
        console.log("[ at.mastermind8.web.action_pickColour.currentScore ][ OK ] currentScore added up, now on: " + currentScore);

        //Check if every point matches the pattern
        let patternMatches = true;
        if (matchingPoints.includes(false))
        {
            patternMatches = false;
        }

        //If the patterns match, display the victory dialog
        if(patternMatches === true)
        {
            //check if the current score is lower than the highscore (lower == better)
            if(currentScore < highscore || highscore == 0)
            {
                highscore = currentScore;
                localStorage.setItem("at.mastermind8.web.highscore", highscore);
            }
            displayDialog("Victory!");
        }

        //If the player filled up the last row AND the patterns don't match, display the loosing dialog
        if(activeRow === 8 && patternMatches === false)
        {
            currentScore = 0;
            displayDialog("You lose!");
            displayPatternOnLose();
        }

        colourPositionHint.forEach(item => {
            //display the hints
            displayPositionHint(item.colour, item.div_id);
            displayColourHint(item.colour, item.div_id);
        });

        activeRow++;                            //if the program reaches this point (all four elements are coloured) than enable the next row for the user
        pattern_try = new Array(4);  //since a new row is started clear the user-picked pattern
    }
}

//reset everything and prepare a new game
function playAgain()
{
    //hide the dialogBox
    let dialogBox = document.getElementById("box_dialog");
    dialogBox.style.visibility = "hidden";

    //reset all coloured points
    for (let i = 1; i <= 32; i++)
    {
        //assemble the ID of the circle
        let circle_play_ID = "circle_play_" + i;

        //get the circle and reset its colour to the default colour (#bbb)
        let circle_play = document.getElementById(circle_play_ID);
        circle_play.style.backgroundColor = "#bbb";
    }

    //reset all hint-points
    for (let i = 1; i <= 32; i++)
    {
        //assemble the ID of the circle
        let box_hint_ID = "box_hint_" + i;

        //get the circle and reset its colour to the default colour (#bbb)
        let box_hint = document.getElementById(box_hint_ID);
        box_hint.style.backgroundColor = "#bbb";
    }

    //initialize a new game
    init();
}

//convert a colour to hex-code (adapted from the internet)
function convertColourToHex(colourValue)
{
    var parts = colourValue.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

    delete(parts[0]);
    for (var i = 1; i <= 3; ++i)
    {
        parts[i] = parseInt(parts[i]).toString(16);
        if (parts[i].length == 1)
        {
            parts[i] = '0' + parts[i];
        }
    }
    return '#' + parts.join('');
}

/*
    display a dialog with a message and a button to start a new game
    text: the text that should be displayed on the dialog
 */
function displayDialog(text)
{
    //get the dialogBox and make it visible
    let dialogBox = document.getElementById("box_dialog");
    dialogBox.style.visibility = "visible";

    //get the text for the message in the dialog and append the given text
    let dialogText = document.getElementById("dialog_text");
    dialogText.append(text + "\n Score: " + currentScore);

    //update the highscore
    let div_highscore = document.getElementById("div_highscore");
    div_highscore.textContent = highscore;
}

/*
    function to get the row where the given element (id) is located in
 */
function getRow(id)
{
    /*
    console.log("[ DEBUG ][ getRow ] id: " + id);
    console.log("[ DEBUG ][ getRow ] row1: " + row1);
    console.log("[ DEBUG ][ getRow ] row2: " + row2);
    console.log("[ DEBUG ][ getRow ] row3: " + row3);
    console.log("[ DEBUG ][ getRow ] row4: " + row4);
    console.log("[ DEBUG ][ getRow ] row5: " + row5);
    console.log("[ DEBUG ][ getRow ] row6: " + row6);
    console.log("[ DEBUG ][ getRow ] row7: " + row7);
    console.log("[ DEBUG ][ getRow ] row8: " + row8);
    */

    if(row1.includes(id))
    {
        return row1;
    }
    else if (row2.includes(id))
    {
        return row2;
    }
    else if(row3.includes(id))
    {
        return row3;
    }
    else if(row4.includes(id))
    {
        return row4;
    }
    else if(row5.includes(id))
    {
        return row5;
    }
    else if(row6.includes(id))
    {
        return row6;
    }
    else if(row7.includes(id))
    {
        return row7;
    }
    else if(row8.includes(id))
    {
        return row8;
    }
    else
    {
        return null;
    }
}

/*
    Every (game-)element has an id containing a string (like 'circle_play_') and a number.
    This function extracts the number from a given id-string
 */
function extractNumberFromId(id)
{
    //get the ID (from the button that should be colored) and split it to have access to the number
    let hint_arr_split = id.split("_");
    //console.log("[ DEBUG ][ extractNumberFromId ] hint_arr_split: " + hint_arr_split);

    ///extract the number of the id
    let hint_id_int = hint_arr_split[2];
    //console.log("[ DEBUG ][ extractNumberFromId ] hint_id_int: " + hint_id_int);

    return hint_id_int;
}

/*
    Function to display the position hint for a given colour and a given elemnent
    colour: the colour the element has been given by the user
    div_id: the id of the element that got coloured
 */
function displayPositionHint(colour, div_id)
{
    console.log("[ at.mastermind8.web.displayPositionHint ][ DEBUG ] (PARAMETER) colour: " + colour);
    console.log("[ at.mastermind8.web.displayPositionHint ][ DEBUG ] (PARAMETER) div_id: " + div_id);

    //get the hint-box with the same ID-Number as the button that should be colored
    let hint_id_int = extractNumberFromId(div_id);
    let hint_id = "box_hint_" + hint_id_int;
    console.log("[ DEBUG ][ displayPositionHint ] hint_id: " + hint_id);

    //get the index of the row which the current button is in
    let row = getRow(hint_id_int);
    let rowID = 0;
    for (let i = 0; i < row.length; i++)
    {
        if (hint_id_int == row[i])
        {
            rowID = i;
        }
    }

    //check if the selected color is the same as the generated pattern
    console.log("[ at.mastermind8.web.displayPositionHint ][ DEBUG ] colour: " + colour);
    console.log("[ at.mastermind8.web.displayPositionHint ][ DEBUG ] pattern[rowID]: " + pattern[rowID]);
    if(colour == pattern[rowID])
    {
        console.log("[ at.mastermind8.web.displayPositionHint ][ OK ] colour matches: " + colour + " : " + pattern[rowID]);

        //get the hint and change its colour to black
        let hintClicked = document.getElementById(hint_id);
        hintClicked.style.backgroundColor = "black";
    }
}

/*
    Function to display the colour hint for a given colour and a given elemnent
    colour: the colour the element has been given by the user
    id: the id of the element that got coloured
 */
function displayColourHint(colour, id)
{
    //check if the user-picked colour is in the generated pattern
    if(pattern.includes(colour))
    {
        //get the hint-box with the same ID-Number as the button that should be colored
        let hint_id = "box_hint_" + extractNumberFromId(id);
        console.log("[ at.mastermind8.web.displayColourHint ][ DEBUG ] hint_id: " + hint_id);

        //get the element of the hint-element that should be coloured
        let hintClicked = document.getElementById(hint_id);

        //check if the colour isn't already coloured in black (which means the position is also correct)
        if(hintClicked.style.backgroundColor != "black")
        {
            hintClicked.style.backgroundColor = "white";
        }
    }
}

/*
    Display the correct pattern on the lose screen
 */
function displayPatternOnLose()
{
    //get the div that holds the pattern
    let dialogPattern = document.getElementById("dialog_colourPattern");
    dialogPattern.style.visibility = "visible";

    //iterate through the pattern
    for(let i = 0; i < pattern.length; i++)
    {
        let idString = "box_colourPattern_" + (i + 1);              //get the id of the circle that should be coloured
        let boxColourPattern = document.getElementById(idString);   //get the circle that should be coloured
        boxColourPattern.style.backgroundColor = pattern[i];        //set the correct colour
    }
}