var hardMode;
var backgroundMusic;

//Initialise the button states when the page is loaded.
function initPage()
{
    console.log("[ at.mastermind8.web.settings.initPage.hardMode ][ *** ] Trying to obtain hardMode-setting from LocalStorage...")
    hardMode = localStorage.getItem("at.mastermind8.web.hardMode");

    console.log("[ at.mastermind8.web.settings.initPage.backgroundMusic ][ *** ] Trying to obtain backgroundMusic-setting from LocalStorage...")
    backgroundMusic = localStorage.getItem("at.mastermind8.web.music");
    //var slider_inactive;

    //check if the hardMode localStorage returned a correct value, if not, set the hardmode to false
    if(hardMode == null)
    {
        hardMode = false;
        console.log("[ at.mastermind8.web.settings.initPage.hardMode ][ WARN ] hardMode-setting couldn't be obtained, using default setting instead");
    }
    else if(hardMode == "false")
    {
        hardMode = false;
    }
    else if(hardMode == "true")
    {
        hardMode = true;
    }
    console.log("[ at.mastermind8.web.settings.initPage.hardMode ][ OK ] hardMode is set to " + hardMode);

    //check if the backgroundMusic localStorage returned a correct value, if not, set the hardmode to false
    if(backgroundMusic == null)
    {
        backgroundMusic = true;
        console.log("[ at.mastermind8.web.settings.initPage.backgroundMusic ][ WARN ] backgroundMusic-setting couldn't be obtained, using default setting instead");
    }
    else if (backgroundMusic == "false")
    {
        backgroundMusic =  false;
    }
    else if (backgroundMusic == "true")
    {
        backgroundMusic = true;
    }
    console.log("[ at.mastermind8.web.settings.initPage.backgroundMusic ][ OK ] backgroundMusic is set to " + backgroundMusic);

    /*
    //get the hardMode-slider to set inactive
    if (hardMode == true)
    {
        slider_inactive = document.getElementById("slider_music");   //get the inactive button
    }
    */
    let slider_hardMode = document.getElementById("slider_mode");
    slider_hardMode.checked = hardMode;

    let slider_music = document.getElementById("slider_music");
    console.log("[ at.mastermind8.web.settings.initPage.backgroundMusic ][ DEBUG ] slider_music is: " + slider_music.checked);
    slider_music.checked = backgroundMusic;
    console.log("[ at.mastermind8.web.settings.initPage.backgroundMusic ][ DEBUG ] slider_music is: " + slider_music.checked);

    //localStorage.setItem("at.mastermind8.web.hardMode", hardMode);
    //localStorage.setItem("at.mastermind8.web.music", backgroundMusic);
}


//function to set the hard-mode when the user changes the "slider"-button
function action_switchHardmode()
{
    let slider_hardMode = document.getElementById("slider_mode");
    hardMode = slider_hardMode.checked; //TODO
    console.log("[ at.mastermind8.web.settings.action_switchHardmode ][ DEBUG ] slider_hardMode is: " + slider_hardMode.checked);

    localStorage.setItem("at.mastermind8.web.hardMode", hardMode);
    console.log("[ at.mastermind8.web.action_switchHardmode ][ OK ] hardMode changed, hard mode: " + backgroundMusic);
}

//function to set the background-music when the user changes the "slider"-button
function action_switchBackgroundMusic()
{
    let slider_music = document.getElementById("slider_music");
    backgroundMusic = slider_music.checked;
    console.log("[ at.mastermind8.web.settings.action_switchBackgroundMusic ][ DEBUG ] slider_music is: " + slider_music.checked);

    localStorage.setItem("at.mastermind8.web.music", backgroundMusic);
    console.log("[ at.mastermind8.web.action_switchBackgroundMusic ][ OK ] backgroundMusic changed, background music on: " + backgroundMusic);
}

//function to reset the highscore
function action_resetHighscore()
{
    let dialog = document.getElementById("dialog_ok");
    dialog.style.visibility = "visible";

    localStorage.removeItem("at.mastermind8.web.highscore");

    /*
    let dialog = document.getElementById("dialog_ok");
    dialog.text = "OK";

     */
}